package com.example.sakishima.bookfinder.view.search;

import com.example.sakishima.bookfinder.model.search.pojo.Item;

import java.util.List;

/**
 * Created by
 *
 * @author Sakishima (12.05.2018)
 */
public interface BookSearchResult {
    void onBookSearchComplete(List<Item> booksList);
    void onBookSearchError(int errorString);
    void showSearchProgress();
    void hideSearchProgress();
}
