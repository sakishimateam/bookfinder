package com.example.sakishima.bookfinder.model.search;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.example.sakishima.bookfinder.R;
import com.example.sakishima.bookfinder.model.search.pojo.Item;

import java.util.List;

/**
 * Created by
 *
 * @author Sakishima (12.05.2018)
 */
public class SearchInteractorImpl implements SearchInteractor, SearchInteractor.ParseGson {
    private OnSearchResult onResult;
    private Context context;

    @Override
    public void doSearchBooks(String query, Context context, final OnSearchResult searchResult) {
        onResult = searchResult;
        this.context = context;
        if (!isConnectedToInternet()) {
            onResult.onSearchError(R.string.no_internet);
            return;
        }
        TakeJson takeJson = new TakeJson();
        takeJson.getJsonString(query, this);
    }

    @Override
    public void parseGsonResult(List<Item> booksList) {
        if (booksList != null) {
            onResult.onSearchComplete(booksList);
        } else {
            onResult.onSearchError(R.string.no_books_found);
        }
    }

    private boolean isConnectedToInternet() {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnectedOrConnecting();
    }
}
