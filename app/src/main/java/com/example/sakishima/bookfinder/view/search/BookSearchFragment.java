package com.example.sakishima.bookfinder.view.search;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.sakishima.bookfinder.R;
import com.example.sakishima.bookfinder.model.search.pojo.Item;
import com.example.sakishima.bookfinder.presenter.search.SearchPresenterImpl;

import java.util.List;

/**
 * Created by
 *
 * @author Sakishima (11.05.2018)
 */
public class BookSearchFragment extends Fragment implements BookSearchResult {
    private static final String TAG = "BookSearchFragment";
    private LinearLayout progressLinearLayout;
    private RecyclerView bookRecyclerView;
    private BooksAdapter bookAdapter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        setRetainInstance(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.options_menu, menu);

        MenuItem searchViewItem = menu.findItem(R.id.search_menu_item);
        final SearchView bookSearchView = (SearchView) searchViewItem.getActionView();
        bookSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                Log.i(TAG, "Search string: " + query);
                SearchPresenterImpl searchPresenter = new SearchPresenterImpl((BookSearchResult) BookSearchFragment.this);
                searchPresenter.newSearchRequest(query, getContext());
                bookSearchView.clearFocus();
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_book_search, container, false);

        bookRecyclerView = view.findViewById(R.id.book_recycler_veiw);
        bookRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        progressLinearLayout = view.findViewById(R.id.search_in_progress);

        return view;
    }

    @Override
    public void onBookSearchComplete(List<Item> booksList) {
        if (bookAdapter == null) {
            bookAdapter = new BooksAdapter(booksList, getActivity());
            bookRecyclerView.setAdapter(bookAdapter);
        } else {
            bookAdapter.setCardList(booksList);
            bookAdapter.notifyDataSetChanged();
            bookRecyclerView.scrollToPosition(0);
        }
    }

    @Override
    public void onBookSearchError(int errorString) {
        Toast.makeText(getContext(), errorString, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showSearchProgress() {
        progressLinearLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideSearchProgress() {
        progressLinearLayout.setVisibility(View.GONE);
    }
}
