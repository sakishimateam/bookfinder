package com.example.sakishima.bookfinder.model.search;

import com.example.sakishima.bookfinder.model.search.pojo.BooksListGson;

import io.reactivex.Flowable;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by
 *
 * @author Sakishima (11.05.2018)
 */
public interface WebApi {
    @GET("/books/v1/volumes")
    Flowable<BooksListGson> getJsonData(
            @Query("q") String question,
            @Query("printType") String printType,
            @Query("projection") String projection,
            @Query("maxResults") Integer maxResults
    );
}
