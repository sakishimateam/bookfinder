package com.example.sakishima.bookfinder.presenter.search;

import android.content.Context;

/**
 * Created by
 *
 * @author Sakishima (12.05.2018)
 */
public interface SearchPresenter {
    void newSearchRequest(String query, Context context);
}
