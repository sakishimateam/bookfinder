package com.example.sakishima.bookfinder.view.search;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.sakishima.bookfinder.R;
import com.example.sakishima.bookfinder.model.search.pojo.Item;
import com.example.sakishima.bookfinder.view.detail.BookDetailActivity;

import java.util.List;

/**
 * Created by
 *
 * @author Sakishima (12.05.2018)
 */
public class BooksAdapter extends RecyclerView.Adapter<BookHolder>{
    private static final String TAG = "BooksAdapter";
    public List<Item> bookList;
    private Activity activity;

    public BooksAdapter(List<Item> bookList, Activity activity) {
        this.bookList = bookList;
        this.activity = activity;
    }

    @NonNull
    @Override
    public BookHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(activity);
        View view = layoutInflater.inflate(R.layout.card_view_layout, parent, false);
        return new BookHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull BookHolder holder, int position) {
        holder.bindCard(bookList.get(position));
    }

    @Override
    public int getItemCount() {
        return bookList.size();
    }

    public void setCardList(List<Item> bookList) {
        this.bookList = bookList;
    }
}
