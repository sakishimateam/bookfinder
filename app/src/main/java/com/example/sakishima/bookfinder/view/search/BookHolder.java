package com.example.sakishima.bookfinder.view.search;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.sakishima.bookfinder.R;
import com.example.sakishima.bookfinder.model.search.pojo.Item;
import com.example.sakishima.bookfinder.view.detail.BookDetailActivity;
import com.squareup.picasso.Picasso;

/**
 * Created by
 *
 * @author Sakishima (12.05.2018)
 */
public class BookHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    private ImageView bookImageView;
    private TextView bookTitleTextView;
    private TextView bookAuthorTextView;
    private TextView bookSubtitleTextView;
    private Item bookInfo;

    public BookHolder(View itemView) {
        super(itemView);
        itemView.setOnClickListener(this);
        bookImageView = itemView.findViewById(R.id.book_thumbnail_image_view);
        bookTitleTextView = itemView.findViewById(R.id.book_title_text_view);
        bookAuthorTextView = itemView.findViewById(R.id.book_author_text_view);
        bookSubtitleTextView = itemView.findViewById(R.id.book_subtitle_text_view);
    }

    public void bindCard(Item bookInfo) {
        this.bookInfo = bookInfo;

        String bookTitle = bookInfo.getVolumeInfo().getTitle();
        String bookAuthor = "";
        if (bookInfo.getVolumeInfo().getAuthors() != null) {
            bookAuthor = TextUtils.join(", ", bookInfo.getVolumeInfo().getAuthors());
        }
        String bookSubtitle = bookInfo.getVolumeInfo().getSubtitle();
        String bookPictureUrl = "";
        if (bookInfo.getVolumeInfo().getImageLinks() != null) {
            bookPictureUrl = bookInfo.getVolumeInfo().getImageLinks().getThumbnail();
        }

        bookTitleTextView.setText(bookTitle);
        bookAuthorTextView.setText(bookAuthor);
        bookSubtitleTextView.setText(bookSubtitle);
        if (!bookPictureUrl.isEmpty()) {
            Picasso.get().load(bookPictureUrl).into(bookImageView);
        }
    }

    @Override
    public void onClick(View v) {
        Context context = v.getContext();
        Intent intent = BookDetailActivity.newIntent(context, bookInfo);
        context.startActivity(intent);
    }
}
