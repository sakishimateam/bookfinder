package com.example.sakishima.bookfinder.presenter.search;

import android.content.Context;
import android.util.Log;

import com.example.sakishima.bookfinder.model.search.SearchInteractor;
import com.example.sakishima.bookfinder.model.search.SearchInteractorImpl;
import com.example.sakishima.bookfinder.model.search.pojo.Item;
import com.example.sakishima.bookfinder.view.search.BookSearchResult;

import java.util.List;

/**
 * Created by
 *
 * @author Sakishima (12.05.2018)
 */
public class SearchPresenterImpl implements SearchPresenter, SearchInteractor.OnSearchResult {
    private static final String TAG = "SearchPresenterImpl";
    BookSearchResult bookSearchResult;
    SearchInteractor searchInteractor;

    public SearchPresenterImpl(BookSearchResult bookSearchResult) {
        this.bookSearchResult = bookSearchResult;
        searchInteractor = new SearchInteractorImpl();
    }

    @Override
    public void newSearchRequest(String query, Context context) {
        bookSearchResult.showSearchProgress();
        searchInteractor.doSearchBooks(query, context, this);
    }

    @Override
    public void onSearchComplete(List<Item> booksList) {
        bookSearchResult.hideSearchProgress();
        bookSearchResult.onBookSearchComplete(booksList);
        Log.i(TAG, "!!! Complete.");
    }

    @Override
    public void onSearchError(int errorString) {
        bookSearchResult.hideSearchProgress();
        bookSearchResult.onBookSearchError(errorString);
        Log.i(TAG, "!!! Error. ");
    }
}
