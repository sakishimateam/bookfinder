package com.example.sakishima.bookfinder.view.search;

import android.support.v4.app.Fragment;

import com.example.sakishima.bookfinder.BasicFragmentActivity;

public class BookSearchActivity extends BasicFragmentActivity {

    @Override
    protected Fragment createFragment() {
        return new BookSearchFragment();
    }
}
