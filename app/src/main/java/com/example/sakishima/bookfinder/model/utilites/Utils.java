package com.example.sakishima.bookfinder.model.utilites;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * Created by
 *
 * @author Sakishima (13.05.2018)
 */
public class Utils {
    private static Gson gson;

    public static Gson getGsonParser() {
        if (null == gson) {
            GsonBuilder builder = new GsonBuilder();
            gson = builder.create();
        }
        return gson;
    }
}
