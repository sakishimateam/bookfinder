package com.example.sakishima.bookfinder.view.detail;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.sakishima.bookfinder.R;
import com.example.sakishima.bookfinder.model.search.pojo.Item;
import com.example.sakishima.bookfinder.model.utilites.Utils;
import com.squareup.picasso.Picasso;

/**
 * Created by
 *
 * @author Sakishima (13.05.2018)
 */
public class BookDetailFragment extends Fragment {
    private Item bookDetail;
    private ImageView bookCoverImageView;
    private TextView bookTitleTextView;
    private TextView bookAuthorTextView;
    private TextView bookPublisherTextView;
    private TextView bookPublishedDateTextView;
    private TextView bookSubtitleTextView;
    private TextView bookDescriptionTextVew;
    private TextView bookSnippetTextView;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getActivity() != null){
            Intent intent = getActivity().getIntent();
            if (intent != null) {
                String jsonBookDetail = intent.getStringExtra(BookDetailActivity.EXTRA_BOOK_DETAIL);
                bookDetail = Utils.getGsonParser().fromJson(jsonBookDetail, Item.class);
            }
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_book_detail, container, false);

        bookCoverImageView = view.findViewById(R.id.book_cover_image_view);
        bookTitleTextView = view.findViewById(R.id.book_detail_title_text_view);
        bookAuthorTextView = view.findViewById(R.id.book_detail_author_text_view);
        bookPublisherTextView = view.findViewById(R.id.book_detail_publisher_text_view);
        bookPublishedDateTextView = view.findViewById(R.id.book_detail_publisheddate_text_view);
        bookSubtitleTextView = view.findViewById(R.id.book_detail_subtitle_text_view);
        bookDescriptionTextVew = view.findViewById(R.id.book_detail_description_text_view);
        bookSnippetTextView = view.findViewById(R.id.book_detail_snippet_text_view);

        if (bookDetail.getVolumeInfo().getImageLinks() != null) {
            if (!bookDetail.getVolumeInfo().getImageLinks().getThumbnail().isEmpty()) {
                Picasso.get().load(bookDetail.getVolumeInfo().getImageLinks().getThumbnail()).into(bookCoverImageView);
            }
        }
        bookTitleTextView.setText(bookDetail.getVolumeInfo().getTitle());
        if (bookDetail.getVolumeInfo().getAuthors() != null) {
            bookAuthorTextView.setText(TextUtils.join(", ", bookDetail.getVolumeInfo().getAuthors()));
        }
        bookPublisherTextView.setText(bookDetail.getVolumeInfo().getPublisher());
        bookPublishedDateTextView.setText(bookDetail.getVolumeInfo().getPublishedDate());
        if (bookDetail.getVolumeInfo().getSubtitle() == null || bookDetail.getVolumeInfo().getSubtitle().isEmpty()) {
            bookSubtitleTextView.setVisibility(View.GONE);
        } else {
            bookSubtitleTextView.setVisibility(View.VISIBLE);
            bookSubtitleTextView.setText(bookDetail.getVolumeInfo().getSubtitle());
        }
        bookDescriptionTextVew.setText(bookDetail.getVolumeInfo().getDescription());
        if (bookDetail.getSearchInfo() != null) {
            bookSnippetTextView.setText(Html.fromHtml(bookDetail.getSearchInfo().getTextSnippet()));
        }

        return view;
    }
}
