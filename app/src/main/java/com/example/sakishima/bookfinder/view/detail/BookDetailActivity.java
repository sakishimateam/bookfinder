package com.example.sakishima.bookfinder.view.detail;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;

import com.example.sakishima.bookfinder.BasicFragmentActivity;
import com.example.sakishima.bookfinder.model.search.pojo.Item;
import com.example.sakishima.bookfinder.model.utilites.Utils;

import java.io.Serializable;

/**
 * Created by
 *
 * @author Sakishima (13.05.2018)
 */
public class BookDetailActivity extends BasicFragmentActivity {
    public static final String EXTRA_BOOK_DETAIL = "com.example.sakishima.bookfinder.extra.bookdetail";

    public static Intent newIntent(Context packageContext, Item item) {
        Intent intent = new Intent(packageContext, BookDetailActivity.class);
        intent.putExtra(EXTRA_BOOK_DETAIL, (Serializable) Utils.getGsonParser().toJson(item));
        return intent;
    }

    @Override
    protected Fragment createFragment() {
        return new BookDetailFragment();
    }
}
