package com.example.sakishima.bookfinder.model.utilites;

/**
 * Created by
 *
 * @author Sakishima (11.05.2018)
 */
public class Constants {
    public static final String BASE_URL = "https://www.googleapis.com";
    public static final String PRINT_TYPE = "books";
    public static final String PROJECTION = "lite";
    public static final Integer MAX_RESULTS = 40;
}
