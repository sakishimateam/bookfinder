package com.example.sakishima.bookfinder.model.search;

import android.net.ConnectivityManager;
import android.util.Log;

import com.example.sakishima.bookfinder.model.search.pojo.BooksListGson;
import com.example.sakishima.bookfinder.model.utilites.Constants;

import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by
 *
 * @author Sakishima (11.05.2018)
 */
public class TakeJson {
    private static final String TAG = "TakeJson";
    private Retrofit retrofit;

    public TakeJson() {
        retrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .baseUrl(Constants.BASE_URL)
                .build();
    }

    public void getJsonString(String query, SearchInteractor.ParseGson result) {
        WebApi webApi = retrofit.create(WebApi.class);
        Flowable<BooksListGson> flowableRetrofit = webApi.getJsonData(
                query,
                Constants.PRINT_TYPE,
                Constants.PROJECTION,
                Constants.MAX_RESULTS
        );
        flowableRetrofit
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<BooksListGson>() {
                    @Override
                    public void onSubscribe(Subscription s) {
                        s.request(Long.MAX_VALUE);
                    }

                    @Override
                    public void onNext(BooksListGson booksListGson) {
                        Log.i(TAG, "--> Retrofit return data. Total count: " + booksListGson.getItems().size());
                        result.parseGsonResult(booksListGson.getItems());
                    }

                    @Override
                    public void onError(Throwable t) {
                        Log.i(TAG, "--> Retrofit return error");
                        result.parseGsonResult(null);
                    }

                    @Override
                    public void onComplete() {
                        Log.i(TAG, "--> Retrofit complete");
                    }
                });
    }
}
