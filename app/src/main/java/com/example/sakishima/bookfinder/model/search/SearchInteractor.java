package com.example.sakishima.bookfinder.model.search;

import android.content.Context;

import com.example.sakishima.bookfinder.model.search.pojo.Item;

import java.util.List;

/**
 * Created by
 *
 * @author Sakishima (12.05.2018)
 */
public interface SearchInteractor {
    interface OnSearchResult {
        void onSearchComplete(List<Item> booksList);
        void onSearchError(int errorString);
    }

    interface ParseGson {
        void parseGsonResult(List<Item> booksList);
    }

    void doSearchBooks(String query, Context context, OnSearchResult searchResult);
}
